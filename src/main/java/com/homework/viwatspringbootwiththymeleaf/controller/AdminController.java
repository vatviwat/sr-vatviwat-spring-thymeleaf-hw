package com.homework.viwatspringbootwiththymeleaf.controller;

import com.homework.viwatspringbootwiththymeleaf.model.Article;
import com.homework.viwatspringbootwiththymeleaf.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class AdminController  {

    private ArticleRepository articleRepository;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @RequestMapping(value="/admin/logout", method=RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "admin/logout";
    }

    @GetMapping()
    public String home(){
        return "admin/home";
    }
    @GetMapping("/admin")
    public String admin(){
        return "admin/home";
    }

    @GetMapping("/admin/login")
    public String login(){
        return "admin/login";
    }


    @GetMapping("/admin/dashboard")
    public String adminDashboard(ModelMap modelMap){
        List<Article> articleList = new ArrayList<>();
        articleList = articleRepository.findAll();
        modelMap.addAttribute("articleList",articleList);
        System.out.println(articleList);
        return "admin/fragments/statistic";
    }

    //TODO: go redirect to admin/dashboard after submit form
    @PostMapping("/admin/add")
    public String saveArticle(@ModelAttribute @Valid Article article, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "admin/fragments/management-page";
        }
        articleRepository.save(article);
        return "redirect:/admin/review-articles";
    }

    //TODO: to post article
    @GetMapping("/admin/article")
    public String viewSaveArticle(@ModelAttribute Article article, ModelMap modelMap){
        modelMap.addAttribute("article",article);
        return "admin/fragments/management-page";
    }

    //TODO: to view list of article
    @GetMapping("/admin/review-articles")
    public String adminTable(ModelMap modelMap){
        List<Article> articleList = new ArrayList<>();
        articleList = articleRepository.findAll();
        modelMap.addAttribute("articleList",articleList);
        System.out.println(articleList);
        return "admin/fragments/reviewing-page";
    }

    //TODO: get article by id
    @GetMapping("/admin/review-articles/{id}")
    public String viewArticleById(@PathVariable("id") int id, ModelMap modelMap){
        Optional<Article> article = articleRepository.findById(id);
        modelMap.addAttribute("article",article);
        return "admin/fragments/reviewing-byId-page";
    }

    //TODO: get article by id for edit
    @PostMapping("/admin/edit-article/{id}/edit")
    public String editArticleById(@PathVariable("id") int id,@ModelAttribute @Valid Article article, BindingResult bindingResult,ModelMap modelMap){
        if(bindingResult.hasErrors()) {
            article.setId(id);
            return "admin/fragments/edit-article";
        }
        modelMap.addAttribute("article", article);
        articleRepository.save(article);
        return "redirect:/admin/review-articles";
    }

    //TODO: to edit article
    @GetMapping("/admin/article/edit/{id}")
    public String editArticleById(@PathVariable("id") int id, ModelMap modelMap){
        Article article = articleRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("id"+id));
        modelMap.addAttribute("article",article);
        return "admin/fragments/edit-article";
    }

    //TODO: to delete article by id
    @GetMapping("/admin/article/delete/{id}")
    public String deleteArticleById(@PathVariable("id") int id, ModelMap modelMap){
        Article article = articleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("id:" + id));
        articleRepository.delete(article);
        modelMap.addAttribute("articles",articleRepository.findAll());
        return "redirect:/admin/review-articles";
    }

}
