package com.homework.viwatspringbootwiththymeleaf.Configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/admin/dashboard").hasRole("ADMIN")
                .antMatchers("/admin/review-articles").hasAnyRole("ADMIN","REVIEWER","EDITOR")
                .antMatchers("/admin/article").hasAnyRole("ADMIN","EDITOR")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/admin/login")
                .permitAll()
                .defaultSuccessUrl("/admin");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authentication) throws Exception {
        authentication.inMemoryAuthentication()
                .withUser("dara")
                .password(passwordEncoder().encode("dara123"))
                .authorities("ROLE_USER")
                .and()

                .withUser("kanha")
                .password(passwordEncoder().encode("kanha123"))
                .authorities("ROLE_EDITOR")
                .and()

                .withUser("reksmey")
                .password(passwordEncoder().encode("reksmey123"))
                .authorities("ROLE_REVIEWER")
                .and()

                .withUser("makara")
                .password(passwordEncoder().encode("makara123"))
                .authorities("ROLE_ADMIN");

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}



